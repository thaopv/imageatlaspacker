export const IMAGE_CONFIG_LEGEND = {
  imageInfo: {
    exportTexture: 'DRAGON',
    cover: {
      width: 1800,
      height: 1400
    }
  },
  PARTS: {
    BEHIND_LEFT_FOOT: {
      zIndex: 0,
      anchor: 1
  },
    FRONT_LEFT_FOOT: {
      zIndex: 0,
      anchor: 1
    },
    WING_LEFT: {
      zIndex: 0,
      anchor: 1
    },
    BODY: {
      zIndex: 1,
      anchor: 1
    },

    CHEST: {
      zIndex: 2,
      anchor: 1
    },
    BACK_SCALE: {
      zIndex: 3,
      anchor: 1
    },
    HEAD: {
      zIndex: 4,
      anchor: 1
    },
    LEFT_HORN: {
      zIndex: 0,
      anchor: 1
    },
    MIDDLE_HORN: {
      zIndex: 6,
      anchor: 1
    },
    RIGHT_HORN: {
      zIndex: 6,
      anchor: 1
    },
    EYES: {
      zIndex: 7,
      anchor: 1
    },
    WING_RIGHT: {
      zIndex: 10,
      anchor: 1
    },
    TAIL: {
      zIndex: 9,
      anchor: 1
    },
    BEHIND_RIGHT_FOOT: {
      zIndex: 8,
      anchor: 1
    },
    FRONT_RIGHT_FOOT: {
      zIndex: 8,
      anchor: 1
    }
  },
  WING_RIGHT: {
    0: { x: 88, y: 65 },
  },
  WING_LEFT: {
    0: { x: 319, y: 73 },
  },
  RIGHT_HORN: {
    1: { x: 1320, y: 236 }
  },
  TAIL: {
    0: { x: 919, y: 890 },
  },
  MIDDLE_HORN: {
    1: { x: 1521, y: 158 },
  },
  EYES: {
    0: {
      0: { x: 1501, y: 336 },
    },
  },
  HEAD: {
    0: { x: 1367, y: 330 },
  },
  BACK_SCALE: {
    0: { x: 1168, y: 206 },
  },
  CHEST: {
    0: { x: 1324, y: 433 },
    
  },
  BODY: {
    0: { x: 1078, y: 379 },
  },
  FRONT_LEFT_FOOT: {
    0: { x: 1460, y: 818 },
  },
  BEHIND_LEFT_FOOT: {
    0: { x: 1217, y: 849 },
  },
  BEHIND_RIGHT_FOOT: {
    0: { x: 1082, y: 886 },
  },
  FRONT_RIGHT_FOOT: {
    0: { x: 1316, y: 785 },
  },
  LEFT_HORN: {
    1: { x: 1360, y: 229 },
  },
};
