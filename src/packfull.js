const dragon_type = {
  METAL: 'METAL',
  WOOD: 'WOOD',
  WATER: 'WATER',
  FIRE: 'FIRE',
  EARTH: 'EARTH'
}

const metalConfig = require('../dragon-part/METAL/config').IMAGE_CONFIG_METAL
const woodConfig = require('../dragon-part/WOOD/config').IMAGE_CONFIG_WOOD
const waterConfig = require('../dragon-part/WATER/config').IMAGE_CONFIG_WATER
const fireConfig = require('../dragon-part/FIRE/config').IMAGE_CONFIG_FIRE
const earthConfig = require('../dragon-part/EARTH/config').IMAGE_CONFIG_EARTH

console.log("Metal Config:", metalConfig)

const PART_IDS = {
  BACK_SCALE: 'BACK_SCALE',
  BODY: 'BODY',
  CHEST: 'CHEST',
  EYES: 'EYES',
  HEAD: 'HEAD',
  RIGHT_HORN: 'RIGHT_HORN',
  LEFT_HORN: 'LEFT_HORN',
  MIDDLE_HORN: 'MIDDLE_HORN',
  TAIL: 'TAIL',
  WING_LEFT: 'WING_LEFT',
  WING_RIGHT: 'WING_RIGHT',
  FRONT_LEFT_FOOT: 'FRONT_LEFT_FOOT',
  BEHIND_LEFT_FOOT: 'BEHIND_LEFT_FOOT',
  BEHIND_RIGHT_FOOT: 'BEHIND_RIGHT_FOOT',
  FRONT_RIGHT_FOOT: 'FRONT_RIGHT_FOOT'
}

const bodyPartRoot = {
  BACK_SCALE: 'backscales',
  BODY: 'body',
  CHEST: 'chest',
  EYES: 'eyes',
  HEAD: 'head',
  RIGHT_HORN: 'horns',
  LEFT_HORN: 'horns',
  MIDDLE_HORN: 'middlehorns',
  TAIL: 'tail',
  WING_LEFT: 'wings',
  WING_RIGHT: 'wings',
  FRONT_LEFT_FOOT: 'body',
  BEHIND_LEFT_FOOT: 'body',
  BEHIND_RIGHT_FOOT: 'body',
  FRONT_RIGHT_FOOT: 'body'
}

const TYPE_ROOT = {
  METAL: './dragon-part/METAL/',
  WOOD: './dragon-part/WOOD/',
  WATER: './dragon-part/WATER/',
  FIRE: './dragon-part/FIRE/',
  EARTH: './dragon-part/EARTH/',
  a: './dragon-part/a/',
  b: './dragon-part/b/',
  c: './dragon-part/c/',
  d: './dragon-part/d/',
  e: './dragon-part/e/'
}

const getImgPartSrc = (type, partID, index, color) => {
  let startIndex = 0
  let rootURL = TYPE_ROOT[type]
  switch (partID) {
    case 'TAIL':
    case 'HEAD':
    case 'EYES':
    case 'CHEST':
    case 'BACK_SCALE': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/${color}.png`
    }
    case 'BODY': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/body.png`
    }
    case 'RIGHT_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${startIndex}/${color}/right.png`
    }
    case 'LEFT_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${startIndex}/${color}/left.png`
    }
    case 'MIDDLE_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${startIndex}/${color}.png`
    }
    case 'WING_LEFT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/${color}/left.png`
    }
    case 'WING_RIGHT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/${color}/right.png`
    }
    case 'FRONT_LEFT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/left_front_leg.png`
    }
    case 'BEHIND_LEFT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/left_back_leg.png`
    }
    case 'FRONT_RIGHT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/right_front_leg.png`
    }
    case 'BEHIND_RIGHT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/${index}/right_back_leg.png`
    }

    default: break;
  }
}

const getSpecialImgPartSrc = (type, partID, index, color) => {
  let startIndex = 0
  let rootURL = TYPE_ROOT[type]
  switch (partID) {
    case 'TAIL':
    case 'HEAD':
    case 'EYES':
    case 'CHEST':
    case 'BACK_SCALE': {
      return `${rootURL}${bodyPartRoot[partID]}/${color}.png`
    }
    case 'BODY': {
      return `${rootURL}${bodyPartRoot[partID]}/body.png`
    }
    case 'RIGHT_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${color}/right.png`
    }
    case 'LEFT_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${color}/left.png`
    }
    case 'MIDDLE_HORN': {
      startIndex = index + 1
      return `${rootURL}${bodyPartRoot[partID]}/${color}.png`
    }
    case 'WING_LEFT': {
      return `${rootURL}${bodyPartRoot[partID]}/${color}/left.png`
    }
    case 'WING_RIGHT': {
      return `${rootURL}${bodyPartRoot[partID]}/${color}/right.png`
    }
    case 'FRONT_LEFT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/left_front_leg.png`
    }
    case 'BEHIND_LEFT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/left_back_leg.png`
    }
    case 'FRONT_RIGHT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/right_front_leg.png`
    }
    case 'BEHIND_RIGHT_FOOT': {
      return `${rootURL}${bodyPartRoot[partID]}/right_back_leg.png`
    }

    default: break;
  }
}
// const buildURLRes = (dragonType, dragonIndex, colorIndex) => {
const buildURLRes = (config) => {
  const {
    dragonType,
    dragonIndex,
    colorIndex,
    head = 0,
    eye = 0,
    tail = 0,
    wing = 0,
    chest = 0,
    backscale = 0,
    horn = 0,
    middlehorn,
  } = config
  let partConfig
  switch (dragonType) {

    case dragon_type.METAL: {
      partConfig = { ...metalConfig }
      break
    }
    case dragon_type.WOOD: {
      partConfig = { ...woodConfig }
      break
    }
    case dragon_type.WATER: {
      partConfig = { ...waterConfig }
      break
    }
    case dragon_type.FIRE: {
      partConfig = { ...fireConfig }
      break
    }
    case dragon_type.EARTH: {
      partConfig = { ...earthConfig }
      break
    }
  }


  for (const partName in partConfig.PARTS) {
    let newIndex = dragonIndex;
    let partIndex = dragonIndex;
    let partConf = partConfig.PARTS[partName]
    switch (partName) {
      case PART_IDS.BACK_SCALE:
        partIndex = backscale;
        break;
      case PART_IDS.EYES:
        partIndex = eye;
        break
      case PART_IDS.HEAD:
        partIndex = head;
        break
      case PART_IDS.TAIL:
        partIndex = tail;
        break
      case PART_IDS.CHEST:
        partIndex = chest;
        break
      case PART_IDS.MIDDLE_HORN:
        partIndex = middlehorn;
        break
      case PART_IDS.LEFT_HORN:
        partIndex = horn;
        break
      case PART_IDS.RIGHT_HORN:
        partIndex = horn;
        break
      case PART_IDS.WING_LEFT:
      case PART_IDS.WING_RIGHT:
        partIndex = wing;
        break
    }
    let src =  getImgPartSrc(dragonType, partName, partIndex, colorIndex)
    if (isNaN(partIndex)) {
        getSpecialImgPartSrc(dragonType, partName, partIndex, colorIndex)
    }
    switch (partName) {
      case 'RIGHT_HORN':
      case 'LEFT_HORN':
      case 'MIDDLE_HORN':
        newIndex = dragonIndex + 1;
        partIndex = partIndex + 1
    }

    if (partName == PART_IDS.EYES) {
      const pos = partConfig[partName][partIndex][head]
      partConf.x = pos.x
      partConf.y = pos.y
    } else {
      const pos = partConfig[partName][partIndex]
      partConf.x = pos.x
      partConf.y = pos.y
    }

    partConf.src = src
  }
  return partConfig;
}

const dragonIndex = process.argv[4] || 0
const colorIndex = 4;// process.argv[3] || 0
const dragonType = dragon_type.WOOD
// console.log("args:", process.argv[2])
const configRES = {
  dragonType,
  dragonIndex,
  colorIndex,
  head: 2,
  eye: 2, //process.argv[2] || 0,
  tail: 2,
  wing: 0,
  chest: 4,
  backscale: 0,
  horn: 0,
  middlehorn: 0,

  "horns": "0",
  "middlehorns": "0",
  "backcales": "0",
  "tail": "2",
  "head": "2",
  "eyes": "2",
  "wings": "0",
  "chest": "4",
  "bodyColor": "4",
  "wingsColor": "4",
  "tailColor": "3"

}
// const config = buildURLRes(configRES)


// console.log('config',  JSON.stringify(config, null, 2))
const buildDragon = require('./index').buildFullDragon
const promise = [5].map(it => {

  let conf = { ...configRES, head: it, wing: it, tail: it, eye: it, backscale: it, chest: it, middlehorn: it, horn: it };
  const newConf = buildURLRes(configRES)
  console.log(`head: ${conf.head}  eye: ${conf.eye} Horn: ${conf.horn}`)
  return buildDragon(newConf, `./NEW_PACK/${dragonType}/${dragonType}_${dragonIndex}_h-${conf.head}_eye-${conf.eye}_${colorIndex}.png`)
})

const doPack = async () => {
  await Promise.all(promise)
}
doPack();

