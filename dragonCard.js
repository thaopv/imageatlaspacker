
let fs = require('fs');
const Jimp = require('jimp')
const coverConfig = {
  1: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/1.jpg'
  },
  2: {
    textColor: 'BLACK',
    src: './DRAGON_COVER/2.jpg'
  },
  3: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/3.jpg'
  },
  4: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/4.jpg'
  },
  5: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/5.jpg'
  },
  6: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/6.jpg'
  },
  7: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/7.jpg'
  },
  8: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/8.jpg'
  },
  9: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/9.jpg'
  },
  10: {
    textColor: 'WHITE',
    src: './DRAGON_COVER/10.jpg'
  }
}

const loadImageToBuffer = async (imageConfig) => {


  const { src: imgURL } = imageConfig
  try {
    if (imgURL.indexOf('http://') != -1 || imgURL.indexOf('https://') != -1) {
      //load from host
      const { data } = await axios.get(imgURL, { responseType: 'arraybuffer' })
      return { contents: data, ...imageConfig };
    } else {
      const imgContent = fs.readFileSync(imgURL)
      return { contents: imgContent, ...imageConfig };
    }
  } catch (ex) {
    console.error("ERROR LOAD IMAGE:", ex)
  }
  return imageConfig;
}

const loadImage = (imgConf) => {
  return new Promise((resolve, reject) => {
    Jimp.read(imgConf.src).then(image => {
      resolve({ ...imgConf, image })
    }).catch(ex => {
      reject(ex)
    });
  })
}
const createEmptyImage = (width, height) => {
  return new Promise((resolve, reject) => {
    new Jimp(width, height, 0x0, function (err, image) {
      if (err) {
        return reject(err)
      }
      resolve(image)
    });
  })
}
const getTextWidth = (text) => {
  return new Promise((resolve, reject) => {
    Jimp.measureText(Jimp.FONT_SANS_32_BLACK, text).then(w => {
      resolve(w)
    }).catch(ex => {
      reject(ex)
    })
  })
}
const measureText = (text, fontName) => {
  return new Promise((resolve, reject) => {
    Jimp.loadFont(fontName).then(font => {
      if (font) {

        var x = 0;
        for (var i = 0; i < text.length; i++) {
          if (font.chars[text[i]]) {
            x += font.chars[text[i]].xoffset
              + (font.kernings[text[i]] && font.kernings[text[i]][text[i + 1]] ? font.kernings[text[i]][text[i + 1]] : 0)
              + (font.chars[text[i]].xadvance || 0);
          }
        }
        resolve(x)
      } else {
        reject(ex)
      }

    }).catch(ex => {
      reject(ex)
    })
  })
}

const createTextImg = (color, x, y, text, textImg) => {
  return new Promise((resolve, reject) => {
    Jimp.loadFont(color == 'WHITE' ? Jimp.FONT_SANS_32_WHITE : Jimp.FONT_SANS_32_BLACK).then(font => {
      if (font) {
        try {
          textImg.print(
            font,
            x,
            y,
            text
          );
          resolve(textImg)
        } catch (ex) {
          console.log("write:", ex)
          reject('Font not found!')
        }


      } else {
        reject('Font not found!')
      }

    });
  })
}

const packDragonWithCard = async (config, saveName) => {
  const { cover, dragon } = config
  const coverImg = await loadImage(cover)
  const dragonImg = await loadImage(dragon)
  const coverImage = coverImg.image;
  let textWidth = await measureText(`Loot Dragon NFT #${dragon.id}`, Jimp.FONT_SANS_32_WHITE);
  const textEmpty = await createEmptyImage(textWidth + 2, 40);

  let textDragonId = await createTextImg(cover.textColor, 2, 0, `Loot Dragon NFT #${dragon.id}`, textEmpty);
  textDragonId = textDragonId.rotate(-90)
  const promiseWrite = [{ image: dragonImg.image, x: 100, y: 100 }, { image: textDragonId, x: 1241, y: 540, isText: true }].map(it => {
    return new Promise((resolve, reject) => {
      const { x, y, isText } = it //getPositionFromAnchor(it.x, it.y, it.image.bitmap.width, it.image.bitmap.height, it.anchor)
      let newY = y
      if (isText) {
        newY = y - it.image.bitmap.height / 2
      }
      coverImage.composite(it.image, x, newY, (err, img) => {
        if (!err) {
          resolve(it);
        } else {
          reject(err)
        }
      })
    })
  })
  await Promise.all(promiseWrite);
  coverImage.write(saveName)
}
const start = async () => {
  try {
    const packParams = {
      cover: coverConfig[1],
      dragon: { id: '112233', src: './DRAGON_COVER/dragons/112233.png' }
    }
    await packDragonWithCard(packParams, './cover1.png')
  } catch (ex) {
    console.log(ex)
  }
}

let arrConfig = new Array(1000).fill(1)
arrConfig = arrConfig.map((it, idx) => {
  console.log("idx:", idx % 9 + 1)
  return {
    cover: coverConfig[idx %  + 1],
    dragon: { id: `${it * 10}`, src: `./DRAGON_COVER/dragons/112233.png` }
  }
})
const recursivePack = async (arrBuff, index) => {
  console.log("Packe index", index, )
  if (index >=  arrBuff.length) {
    console.log("END TIME", new Date().getTime())
    return 
  }
  try {
    const packParams = arrBuff[index]
    if (!packParams) {
      debugger
    }
    await packDragonWithCard(packParams, `./covers_all/cover_${index}.png`)
    // console.log(`./covers_all/cover_${index}.png`)
    recursivePack(arrBuff, ++index)
  } catch (ex) {
    console.log(ex)
  }

}
console.log("Begin Time", new Date().getTime())
// recursivePack(arrConfig, 0)
// start()